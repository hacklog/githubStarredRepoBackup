#githubStarredRepoBackup

1. How to build
clone this repo and go to the repo dir.
run 
```bash
go build .
```

2. Configuration

firt ,make a copy of config.sample.ini :
```bash
cp ./config.sample.ini config.ini
```

then edit config.ini use your favorite editor.

sample config:
```ini
[github]
#the github account (which suspended by github is also OK) need to backup
user = ihacklog
start_page = 1
per_page = 100
sleep = 800
#the oauth token of your new github account
# get token from https://github.com/settings/tokens/new
token = 
# set it to false if you also want to save star info to mongodb
star_only = true

#mongodb info is only required when star_only = false
[mongo]
user = huangyewudeng
passwd = passw0rd
host = 127.0.0.1
port = 27017
db_name = github
collection = starred
```

3. run it

```bash
./githubStarredRepoBackup
```